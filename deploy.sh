#!/bin/bash
if [[ `git status --porcelain` ]]; then
  echo "** Uncommitted changes detected. **" 
  echo "Please commit or stash and run deploy again."
else
  mkdocs build
  rsync -r site/* www.corealisation.com:/var/www/www.rightsdriven.org
fi
