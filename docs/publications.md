# Publications

## 2023

In preparation: *Representing Rights:
A Lingua Franca for Rights in Software Engineering*


## 2022

<a href="https://carrcenter.hks.harvard.edu/alexandervoss_22">
![Cover of the Carr Center working paper in which I ask if we can move
fast without breaking things.](/imgs/22_voss.png){ align="left" }
</a>

Voss, Alexander. <a href="https://carrcenter.hks.harvard.edu/alexandervoss_22">Can 
We Move Fast Without Breaking Things? Software Engineering Methods Matter to Human 
Rights Outcomes.</a> Carr Center Discussion Paper Series, 
October 24, 2022. 

*Abstract:* As the products of the IT industry have become ever more
prevalent in our everyday lives, evidence of undesirable consequences
of their use has become ever more difficult to ignore. Consequently,
several responses ranging from attempts to foster individual ethics
and collective standards in the industry to legal and regulatory
frameworks have been developed and are being widely discussed in the
literature. This paper instead makes the argument that currently
popular methods of software engineering are implicated as they hinder
work that would be necessary to avoid negative outcomes. I argue that
software engineering has regressed and that introducing rights as a
core concept into the ways of working in the industry is essential for
making software engineering more rights-respecting.
