---
draft: false
date: 2023-08-04
authors: 
  - avoss
categories:
  - due diligence
  - agile
---

# A Bias for Due Diligence

"A bias for action" is a motto often heard from adherents of 'Agile'
practices[^agile] It is a call to develop systems by learning from
mistakes made, revising, adjusting things as necessary. Some liken
software engineering to 'science' because they can attach the label
'experiment' to the process of deploying code and gathering data
its effects. Appealing as this rhetoric is, it 
misses and important point or two... 

<!-- more -->

Let's start with the 'software engineering as science' metaphor. Any
scientist worth their salt takes stock of what is already known before
they proceed. They plan their experiments, taking into account factors
such as whether and how they can control confounding variables and, if
people are involved, whether what they are planning to do is ethical.

Unless what is being designed is some control system far removed from
any user and with no conceivable negative impact on people, we had
better use the metaphor 'software engineering as social research'.
Note how I avoid the term 'science' here. 

Making this move brings into view that when we release software, we
impact peoples' lives, sometimes in small ways, sometimes in
significant ways. Sometimes we improve peoples' lives, sometimes we
harm people. Most times we benefit some while negatively
affecting others. Clearly, we would want to avoid negative impacts.

Some impacts are worth avoiding at all cost, especially those that
affect peoples' fundamental rights. Does this mean we need to be
stymied by endless deliberations? No, it does not. What we need to do
is to identify risks and then proceed with appropriate care, having
consulted others who can help and being transparent about what we are
doing. Instead of a "bias for action" we need a "bias for due
diligence".

This is what rights-driven development is about. It is about making
the process of doing *human rights due diligence*[^hrdd] in software 
engineering easier so that we can move forward in the knowledge that
we have done our duty to prevent negative consequences. This way, we
prevent negative impacts on individuals' rights and on wider society.
We also protect our own interest as we avoid lawsuits or losing our 
'social license to operate'[^social_license].

[^agile]: The [manifesto for agile software 
development](https://agilemanifesto.org/) aimed to overcome overly
rigid working practices in software engineering, where in the late
1990s phased process models dominated that often led 'analysis
paralysis'. Somewhat ironically, in trying to overcome the old dogma
of 'milestones' and 'sign-off', it gave rise to a veritable industry 
selling a new dogma, that of Scrum and its 'rituals'. Some of the
authors of the manifesto complain about this state of affairs. [Martin
Fowler, for example, has coined the expression 'Agile industrial
complex'](https://martinfowler.com/articles/agile-aus-2018.html). [Dave
Thomas points out how 'agile', an adjective, has been turned into a
noun, a proper noun no less, that can be
sold.](https://www.youtube.com/watch?v=a-BOSpxYJ9M)

[^hrdd]: The concept of human rights due diligence was included in
the [UN Guiding Principles for Business and Human 
Rights](https://www.business-humanrights.org/en/big-issues/un-guiding-principles-on-business-human-rights/). 
A related concept is human rights impact assessment, see
[Human rights impact assessment of digital activities](https://www.humanrights.dk/publications/human-rights-impact-assessment-digital-activities)
produced by the [Danish Institute for Human
Rights](https://www.humanrights.dk/).

[^social_license]: John Ruggie mentions the concept in his book about
the origins of the UN Guiding Principles, [Just Business:
Multinational Corporations and Human
Rights](https://wwnorton.com/books/Just-Business/).

