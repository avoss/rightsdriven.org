# License

<a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
![](/imgs/by-nc-sa.svg){ class="right" }
</a>

The material on this website is published under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International
License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

Please consult the license document linked to above to inform yourself
about the conditions of the license before reusing the material on
this website. 

Please note that the license granted does not permit use
for commercial purposes. If you wish to use the material for
commercial purposes, please [contact us about obtaining a commercial
license](/about).

## Scraping and Machine Learning

We explicitly do not permit use of content on this site to be used in
commercial machine learning applications as I do not wish it to be put
through a sausage machine and enclosed. See [this blog
post](/blog/2023/07/05/licensing-and-generative-ai/)
for more information.

