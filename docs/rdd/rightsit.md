# Human Rights and IT

:material-typewriter: still writing...

<!--
In 2003, 180 governments affirmed the applicability of the Universal
Declaration of "human rights and fundamental freedoms"


There is no question, then, that human rights apply in
the domain of digital systems, services and platforms. 
Whether there are additional rights that emerge from the
existence and state of information technologies is open
to debate (Jorgensen 2017). 


The European Court of Human Rights (EUCtHR), in a 
number of decisions, has ruled that the European Convention
on Human Rights (ECHR) is a "living document" (Andrew 2021)
and that, therefore, ??? 


---

It is important that our understanding of rights remain
independent of the state of development of specific 
technologies, systems, services and platforms (Andrews 2021)
but at the same time remain open to evolution that tracks
wider trends of technological development.

Andrews speaks of "the importance of the notion of 
'technological neutrality' in laws that intend to regulate 
an open-ended process, where their provisions necessarily
incorporate sufficient resolution, clarity and distinction,
while also allowing the scope of unanticipated advances in
digital technologies" (p.4-5)

"As much of human activity continues to shift online,
affording significant scope for the enjoyment of human
rights, the capacity of different actors to infringe those
rights are also considerable." (p.5)

"too little discussion as to how governments (and industry) can better
furnish individuals and communities with the knowledge and resources
to empower them to more effectively and proactively shape the
implementation and evolution of digital technologies in society"
-->
