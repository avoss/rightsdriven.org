---
title: About
---


RightsDriven.org was created by Alex Voss but is open to anyone
interested in work at the intersection of technology and society.
The aim is to foster a community of people working on rights
within software engineering - to bend the future towards a more
wholesome use of IT.

## Contact

**Alex Voss**<br/>
:material-email: [alex@corealisation.com](mailto:alex@corealisation.com)<br/>
:simple-mastodon: [@AlexVoss@fosstodon.org](https://fosstodon.org/@AlexVoss)

## Feedback

Please help with the development of the rights-driven approach. If you
find anything wrong with the content of the website or if you would
like to request a feature, please use the [issue
tracker](https://codeberg.org/avoss/rightsdriven.org/issues).
Alternatively, contact me using email or on Mastodon (see above).

## About the logo and artwork

The logo is a simple adaptation of the [Human Rights Logo by Predrag
Stakić](https://www.humanrightslogo.net), with the acronym "RDD"
added in the larger version.
