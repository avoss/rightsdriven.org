# RightsDriven.org

This repository contains the source code for the website at
[www.rightsdriven.org](https://www.rightsdriven.org). The site is built using
[MkDocs](https://www.mkdocs.org/) using [Material for
MkDocs](https://squidfunk.github.io/mkdocs-material/) in the [Insiders
edition](https://squidfunk.github.io/mkdocs-material/insiders/).

## Server config

### logrotate

Log rotation is configured in `/etc/logrotate.d/nginx` and is
configured to keep logs for 14 days before deleting them.

