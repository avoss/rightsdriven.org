# Rights-Driven Development

**Rights-driven development**, or "**RDD**" for short, is the idea of letting
peoples' rights drive software development. It comes out of a critique
of the state of contemporary software engineering, which favors speed
of development and avoids any kind of up-front analysis.

It is all well and fine to point out that many things cannot
effectively be known before development starts. Some things can.
What rights people have is one thing that can and should be analysed 
before sitting down to cut code. Of course, what rights a system may
affect may not be entirely clear before development starts, so any 
impact assessment should be updated as the system under development
takes shape.

RDD provides a **language** for this as well as **working practices** that
help integrate rights impact assessments with contemporary software
engineering practices. The language allows rights to be specified
using a domain-specific language, which is at the same time easy to
read and write and structured so that it can be processed
automatically.

