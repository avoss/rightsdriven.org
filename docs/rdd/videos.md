
# Videos

Videos about RDD are available on different platforms to suit your
taste. This also avoids the use of cookies and other trackers that would
be the result of embedding the videos. Choose the platform you prefer.
All links open in a new window or tab.

## Introduction
 
![video still](/imgs/rightsdriven.png){ class="video-still" }
Alex provides a short introduction to the motivation behind rights-driven
development and shows how the Torshi language can help in ensuring
that software developed is rights-respecting.

View on [Peertube](https://spectra.video/w/s2JswUQTxPaqveQfdSKhmG)


## Process

Videos about the outlining the process of rights-driven development.
They show how the practice can be integrated with modern software
engineering practices and how this integration can be modelled.

## Torshi 

[Torshi](/projects/torshi) is a language for rights-driven development
(and more!) that supports teams in letting rights considerations drive
their design decisions. The videos in this section show how the Torshi
workbench is installed and used. They provide examples of
rights-driven development in action.


### Installation 

A walkthrough showing how to install the Torshi workbench. Choose the
version that matches your operating system.

=== "Windows"

    View on [Youtube](#) or [Download](#).

=== "MacOS"

    View on [Youtube](#) or [Download](#).

=== "Linux - Debian-style"

    View on [Youtube](#) or [Download](#).

=== "Linux - Red Hat style"

    View on [Youtube](#) or [Download](#).


</div>

