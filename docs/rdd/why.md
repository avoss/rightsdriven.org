# Why RDD?

The widespread use of information technologies has, no doubt,
significantly impacted our lives in the last decade or two. While many
are enthusiastic about the prospects of further spread of digital
devices, services and platforms, there is an increasing number of
voices that raise concerns. Hardly a day goes by that we do
not read about cautionary tales of how IT has harmed the rights of
people. 

"...they ignore the very real harm done by their products on a in
the here and now."
{: class="quote right"}

Regulators have taken note, not least since the widespread diffusion
of data-driven applications and machine learning. Efforts are well
underway to subject the IT industry to the kind of regulation that it
has long attempted to avoid. Interest in "tech ethics" is surging. At
the same time, the frantic speed of innovation seems to causing even
some captains of the IT industry to think again - though they locate
the risks in the far future where an all-powerful AI threatens the
very existence of humanity. At the same time, they tend to ignore the 
very real harm done by their products in the here an now.

"...it does not take whizzbang AI technologies to adversely affect the
rights of people"
{: class="quote left"}

It is no secret that it does not take whizzbang AI technologies to
adversely affect the rights of people, especially of those who are, in
one way or another, more vulnerable than we would all like to be.
Much more mundane systems have been used for a long time to
discriminate and exclude, to exploit or simple to ignore the legitimate
interests of people. There have been reactions to this, not least in
the form of various drives to make computing "more human-centric" such
as participatory design. 

"...cross-cutting and longer-term concerns are all too easily
sidelined..."
{: class="quote right"}

However, contemporary software engineering practices have helped to
significantly increase the speed of development. This is a good thing
as it saves time and resources and decreases the time from initial
development to gaining experience with a design. No longer do we need
to wait for months or years before we get feedback. What is less
fortunate, though, is that the proponents of agile development methods
eschew and kind of up-front analysis. This means that cross-cutting
and longer-term concerns are all too easily sidelined.


Rights-driven Development aims to integrate the concept of rights into
contemporary software engineering practice. It is a practice that 
helps to implement the duties that corporations have under the [UN 
Guiding Principles on Business and Human
Rights](https://www.business-humanrights.org/en/big-issues/un-guiding-principles-on-business-human-rights/).
Read more about it and the [business and human rights movement](bhr.md) to 
learn more about what obligations businesses today have with regard to
human rights.
