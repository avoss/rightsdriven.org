# Business and Human Rights

When we think of human rights we usually think of rights that need to
be protected in relation to those who wield political power. The
earliest human rights documents defined rights that people had in
relation to monarchs. The modern human rights regime came into being
partly in response to the atrocities of the Nazis before and during
World War II. 

More recently, corporate power has also moved into focus.
The operations of corporations, especially in extractive industries 
such as mining and oil production have significant impacts on local
populations. 

## Origins

It is not surprising, then that the global Business and Human Rights
(BHR) movement has its origins in struggles by local people against
environmental destruction and human rights abuses by multinationals.
Most notable are the struggles of the Ogoni people in Nigeria against
the effects of oil extraction and of the victims of the 
[Bhopal disaster](https://en.wikipedia.org/wiki/Bhopal_disaster).

The struggle against apartheid in South Africa was another key factor. The
[Reverend Leon Sullivan](https://en.wikipedia.org/wiki/Leon_Sullivan)
drafted a set of principles for US companies investing in apartheid-era 
South Africa that aimed to counteract some of the human rights
violations that non-white people suffered. 

[^csr]: These early codes of conduct had an impact on the movement 
promoting [corporate social
responsibility](https://en.wikipedia.org/wiki/Corporate_social_responsibility).

## Formation of International Law

The current instruments have evolved in a number of contexts and
over time. The [OECD Guidelines for Multinational
Enterprises](https://mneguidelines.oecd.org/) were established in 
the 1970s and updated on a regular basis. The National Contact Points
for Responsible Business Conduct are national institutions that seek
to promote the Guidelines and related due diligence practices. They
also serve as non-judicial grievance mechanisms, thereby providing
access to remedy for those affected by the impacts of business
conduct.

:material-typewriter: still writing...

<!--
the system of National Contact Points as a system of complaint and
mediation procedures.

UN Global Compact (UNGC)

The UN Guiding Principles 

both soft law but not without effects!

ILO Labor Standards

[ISO 26000](https://www.iso.org/iso-26000-social-responsibility.html)




## Corporate Reporting

non-financial reporting
sustainability reporting such as under 
environmental, sustainability and governance (ESG) reporting

Take, for example, the Corporate Sustainability Reporting Directive
(CSRD) of the European Union. It aims to harmonize reporting of
sustainability information and make it measurable and auditable.
Reports are to be made public to achieve transparency in service of a
number of goals relevant to corporations themselves, to investors,
customers, suppliers, other partners, non-government organizations, civil society, 

-->

## Further Reading

Wettstein, Florian. [Business and Human Rights: Ethical, Legal, and Managerial 
Perspectives](https://www.cambridge.org/us/academic/subjects/management/business-ethics/business-and-human-rights-ethical-legal-and-managerial-perspectives). 
Cambridge, United Kingdom ; New York, NY: Cambridge University Press, 2022.


