# Privacy Policy

Our privacy policy aims to minimize data collection and use of
cookies. 

:material-cookie-off:
We use **no tracking** cookies or other tracking mechanisms. Tracking
cookies are those that store a unique identifier for the user. They
are commonly used in monitoring user behavior and in marketing. We 
have chosen to do neither as a matter of principle. 

:material-cog:
Some websites use **technical cookies to store state** such as user
preferences. Such cookies do not track an individual as the 
information stored is simple settings that cannot serve as an
identifier and so do constitute personal identifying information. 
At the moment, this website also does not use any of these cookies 
but it might in the future, in which case this policy will be changed
and a note be added to the site to alert you of the use of such
cookies.

:octicons-cloud-24:
The website is hosted on a dedicated server on Amazon's AWS cloud. It
does not include content from other sites. All the required content is
hosted on rightsdriven.org itself, including all code and any fonts,
icons and images used.

:material-text-long:
The server that hosts the website keeps an **access log** that includes
the IP address. These log files are used to create statistics about 
website access patterns but IP addresses are resolved only to country 
of origin and not to any more specific location. Access logs are 
automatically deleted after 14 days.


