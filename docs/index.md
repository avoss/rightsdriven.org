---
template: home.html
---

# Rights-Driven Development

![Human rights logo, resembling both a dove and a hand, with the acronym "RDD" overlaid](/imgs/logo.svg){: class="right"}

We have dramatically increased the speed of development and can
deliver updates to our customers continuously. Now we 'just' need to
make sure that what deliver actually adds value, has no negative
consequences, and respects human rights.
Businesses should *know* that they are not harming
peoples' rights and they should be able to *show* that this is the
case.

Rights-driven development consists of a a language for expressing
rights as well as a number of practices and tools. Together, they help
achieve this aim by explicitly including a conception of rights in the
software engineering process. Not only can you ensure that your
software is more rights-respecting, the tools also allow you to show
it! 

&rightarrow; Sounds like a good idea? [Read on...](rdd/index.md) 

&rightarrow;  Not convinced? [Read on about why you should be
interested](rdd/why.md) and [why you should care as a 
business](rdd/bhr.md)...

!!! announce "Early adopters wanted!"
    If you are interested to be an early adopter of RDD, please 
    [get in touch with Alex Voss](/contact).

