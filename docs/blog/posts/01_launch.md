---
draft: false
date: 2023-05-22
categories:
  - announce
authors:
  - avoss
---

# Hello world!

Today I am launching RightsDriven.org, a website to host work on
rights-driven development. The idea of rights-driven development
has been long in the making and I am excited to get the word out.

<!-- more -->

<a href="https://carrcenter.hks.harvard.edu/alexandervoss_22">
![Cover of the Carr Center working paper in which I ask if we can move
fast without breaking things.](/imgs/22_voss.png){ align="left" }
</a>

As the products of the IT industry have become ever more prevalent 
in our everyday lives, evidence of undesirable consequences of their 
use has become ever more difficult to ignore. Consequently, several 
responses ranging from attempts to foster individual ethics and 
collective standards in the industry to legal and regulatory frameworks 
have been developed and are being widely discussed in the literature. 

In a [paper for the Carr Center discussion paper 
series](https://carrcenter.hks.harvard.edu/alexandervoss_22) I instead
make the argument that currently popular methods of software engineering 
are implicated as they hinder work that would be necessary to avoid 
negative outcomes. I argue that software engineering has regressed and 
that introducing rights as a core concept into the ways of working in 
the industry is essential for making software engineering more 
rights-respecting.

Rights-driven development is a first attempt to answer the challenge
by making available a language that allows us to express rights in a
way that it can be linked directly to implementation measures. The 
language is currently under development and I am looking for people
who would be willing to test-drive it with me, starting ca. 1st June
2023. Please get in touch by email, see [contacts info](/contact).
