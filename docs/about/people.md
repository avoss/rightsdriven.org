# People

This website is run by Dr Alex Voss but contains contributions by
others. Those who made substantial contributions have these
acknowledged on the relevant pages and, if they wish this are listed
as contributors below.


![face shot of Alex Voss](/imgs/avoss.png){ class="leftsmall" }

Alex is an academic with a PhD in Informatics (aka. Computer Science)
from Edinburgh University. He has worked at Edinburgh, Manchester
University and St Andrews University. He has held visiting posts at
these institutions as well as Harvard University. He currently works
as an independent researcher and freelancer and focuses on the
development of rights-driven development and products around this
idea.

## Contributors

Please [get in touch if you would like to contribute](/about).
We can talk about forms of acknowledgement and ownership as I do not yet
have a solid contribution model. If you gave resources hosted
elsewhere that you think I should point to, please also drop me a
line.
