---
title: Torshi
---
![torshi logo](/imgs/torshi_logo_transparent.png){ class="right-wide" }

# Coming soon!

Torshi is a language for rights-driven development. It allows rights
specifications to be written and linked to specifications of system
functionality used by software developers. Because IT systems rarely
consist only of hardware and software but involve people as well, 
rights specifications can also link to policies for operating the
system.

## A Domain-Specific Language

Torshi is being developed using the [MPS Meta Programming System](https://www.jetbrains.com/mps/)
by [JetBrains](https://www.jetbrains.com/). It is a domain-specific
language that comes with a projectional editor that guides the user
through the process of constructing rights specifications.

**place example video about here?!**

## Why 'Torshi'?

Torshi is inspired by the [Gherkin](https://cucumber.io/docs/gherkin/) 
language used in Behavior-driven Design. Torshi (ترشی) in Persian means 
'pickle' and [describes a wide variety of sour preserved fruit and
vegetables](https://persiangood.com/recipes/best-5-persian-torshis-ever-pickled-vegetables-fruits/).
Torshi extends what can be expressed to go beyond functional behaviour
(the 'gherkins', if you like) to express rights - and perhaps other 
concepts in the future.
