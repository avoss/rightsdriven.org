# Timeline

| Year |   |
| ---- | - |
| 1760 BCE | Code of Hamurabi |
| 6th century BCE | Cyrus Cylinder |
| c. 528 BCE - 486 BCE | Gautama Buddha |
| 1948 | Universal Declaration of Human Rights |
| 1951 | Convention Relating to the Status of Refugees |
| 1965 | Convention on the Elimination of All Forms of Racial Discrimination|
| 1966 | International Covenant on Civil and Political Rights |
| 1979 | Convention on the Elimination of All Forms of Discrimination Against Women |
| 1984 | Convention Against Torture and Other Cruel, Inhuman or Degrading Treatment or Punishment |
| 1989 | Convention on the Rights of the Child |
| 2006 | Convention on the Rights of Persons with Disabilities |
| 2007 | Declaration on the Rights of Indigenous Peoples |
| 
